const { Games, Users, Biodatas, Scores } = require("../../models");

exports.game = async (req, res) => {
  try {
    const data = await Games.findAll();
    res.send({ data });
  } catch (error) {
    res.send(error);
  }
};

exports.gamedetail = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await Games.findOne({ where: { id } });
    res.send({ data });
  } catch (error) {
    res.send(error);
  }
};

exports.isPlayedGame = async (req, res) => {
  const id = req.user.id

  const user = await Users.findOne({
    where: {id},
    include: Biodatas
  })

  const biodata_id = user.Biodata.id

  const score = await Scores.findAll({
    where: {biodata_id}
  })

  res.json({
    data: score
  })
}
