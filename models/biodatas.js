'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Biodatas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Biodatas.belongsTo(models.Users, {
        foreignKey: "user_id",
      });
      models.Biodatas.hasOne(models.Scores, {
        foreignKey: "biodata_id",
      });
    }
  }
  Biodatas.init(
    {
      name: {
        type: DataTypes.STRING,
        defaultValue: null
      },
      gender: {
        type: DataTypes.STRING,
        defaultValue: null
      },
      address: {
        type: DataTypes.STRING,
        defaultValue: null
      },
      user_id: {
        type: DataTypes.INTEGER,
        defaultValue: null
      },
      date_of_birth: {
        type: DataTypes.DATE,
        defaultValue: null
      },
    },
    {
      sequelize,
      modelName: "Biodatas",
    }
  );
  return Biodatas;
};