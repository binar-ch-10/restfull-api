const { Users, Biodatas } = require("../../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

exports.login = async (req, res) => {
  const { email, password } = req.body;
  const user = await Users.findOne({
    where: { email },
    include: Biodatas,
  });

  if (!user) {
    res.status(402).json({
      result: "Failed",
      message: "username not found!",
    });
    return
  } else {
    validPass = await bcrypt.compare(password, user.password);
    if (!validPass)
      res.status(401).json({
        result: "Failed",
        message: "password incorrect!",
      });
    else {
      const { email, username, Biodata, id } = user;
      const { name } = Biodata;
      const token = jwt.sign(
        {
          id,
          email,
          username,
        },
        "secret"
      );

      const response = {
        username,
        accessToken: token,
      };
      res.send(response);
    }
  }
};
