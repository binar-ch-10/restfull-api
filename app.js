const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const passport = require("./lib/passport");
const authenticationController = require("./controller/authentication");
const gameListController = require("./controller/gamelist/index");
const gameinfo = require("./controller/information/index");
const gameController = require("./controller/games");
const userController = require("./controller/user");
const regisController = require("./controller/register");
const profileController = require("./controller/profile");

const app = express();
const port = process.env.PORT || 8082;

app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());

app.post("/login", authenticationController.login);
app.post("/register", regisController.register);
app.post(
  "/updateprofile",
  passport.authenticate("jwt", { session: false }),
  profileController.updateprofile
);
app.post(
  "/createbiodata",
  passport.authenticate("jwt", { session: false }),
  profileController.createbiodata
);
app.post(
  "/games/rock-paper-scissor",
  passport.authenticate("jwt", { session: false }),
  gameController.rockPaperScissor
);

app.get("/gamelist", gameListController.game);
app.get("/gamelist/detail/:id", gameListController.gamedetail);
app.get("/information", gameinfo.info);
app.get(
  "/gamelist/played-game",
  passport.authenticate("jwt", { session: false }),
  gameListController.isPlayedGame
);
app.get(
  "/profile",
  passport.authenticate("jwt", { session: false }),
  profileController.profile
);
app.get(
  "/scoreprofile",
  passport.authenticate("jwt", { session: false }),
  profileController.scoreprofile
);
app.get(
  "/user",
  passport.authenticate("jwt", { session: false }),
  userController.user
);

app.put(
  "/games/rock-paper-scissor/update-score",
  passport.authenticate("jwt", { session: false }),
  gameController.updateScoreRockPaperScissor
);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
