const { Users } = require("../../models");

exports.user = async (req, res) => {
  const user = await Users.findOne({
    where: { id: req.user.id },
  });

  const { username, email } = user;

  res.json({
    message: "SUCCESS",
    data: { username, email },
  });
};
