'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Users.hasOne(models.Biodatas, {
        foreignKey: "user_id",
      });
    }
  }
  Users.init(
    {
      email: {
        type: DataTypes.STRING,
        unique: {
          args: true,
          msg: "email already taken",
        },
        validate: {
          notEmpty: {
            args: true,
            msg: "email is required",
          },
        },
      },
      username: {
        type: DataTypes.STRING,
        unique: {
          args: true,
          msg: "username already taken",
        },
        validate: {
          notEmpty: {
            args: true,
            msg: "username is required",
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      verified: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Users",
    }
  );
  return Users;
};