const { Informations } = require("../../models");

exports.info = async (req, res) => {
  try {
    const data = await Informations.findAll();
    res.send({ data });
  } catch (error) {
    res.send(error);
  }
};
