const { Users, Biodatas } = require("../../models");
const bcrypt = require("bcrypt");

exports.register = async (req, res) => {
  const payload = req.body;
  const { username, email } = req.body;

  const encryptPassword = await bcrypt.hash(payload.password, 10);
  payload.password = encryptPassword;
  
  const validateEmailUser = await Users.findOne({
    where: { email },
  });

  const validateUsernameUser = await Users.findOne({
    where: { username },
  });

  if (validateEmailUser) {
    res.status(401).json({
      result: "failed",
      message: "email sudah ada",
    });
    return;
  }
  if (validateUsernameUser) {
    res.status(402).json({
      result: "failed",
      message: "username sudah ada",
    });
    return;
  }

  const regisUser = await Users.create(payload);
  const regisBiodata = await Biodatas.create({
    name: null,
    gender: null,
    address: null,
    date: null,
    user_id: regisUser.id,
  });

  res.send({
    username: regisUser.username,
    email: regisUser.email,
    user_id: regisBiodata.user_id,
  });
};
