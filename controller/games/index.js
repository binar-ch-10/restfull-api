const { Users, Biodatas, Scores } = require("../../models");

exports.rockPaperScissor = async (req, res) => {
  const { game_id } = req.body;
  const user = req.user;

  const biodataUser = await Biodatas.findOne({
    where: {user_id: user.id}
  })

  const isAlreadyPlay = await Scores.findOne({
    where: { game_id, biodata_id: biodataUser.id },
  });

  if (!isAlreadyPlay) {
    const newScore = await Scores.create({
      biodata_id: biodataUser.id,
      game_id,
      score: 0,
    });

    newScore.status = false

    res.json({
      message: "SUCCESS",
      data: newScore,
    });
  } else {
    isAlreadyPlay.status = true
    res.json({
      message: "SUCCES",
      data: isAlreadyPlay,
    });
  }
};

exports.updateScoreRockPaperScissor = async (req, res) => {
  const bioUser = await Biodatas.findOne({
    where: { user_id: req.user.id },
  });

  if (!bioUser) {
    res.status(404).json({
      message: "FAILED",
    });
    return;
  }

  const scoreUser = await Scores.findOne({
    where: { biodata_id: bioUser.id, game_id: req.body.game_id },
  });

  if (!scoreUser) {
    res.status(404).json({
      message: "FAILED",
    });
    return;
  }

  await scoreUser.update({ score: Number(req.body.score) });
  await scoreUser.save();

  res.json(scoreUser);
};
