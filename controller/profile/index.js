const { Users, Biodatas, Scores, Games } = require("../../models");

exports.profile = async (req, res) => {
  const user = await Users.findOne({
    where: { id: req.user.id },
    include: Biodatas,
  });

  if (!user) {
    res.status(401).json({
      result: "failed",
      message: "Not find User!",
    });
    return;
  }
  const { name, gender, address, date_of_birth } = user.Biodata;
  const { email, username } = user;
  const payload = {
    name,
    gender,
    address,
    email,
    username,
    date_of_birth,
  };

  res.status(200).json({
    data: payload,
    message: "Success",
  });
};

exports.scoreprofile = async (req, res) => {
  try {
    const biodataUser = await Biodatas.findOne({
      where: { user_id: req.user.id },
      include: Scores,
    });

    if (biodataUser) {
      const scoreUser = await Scores.findAll({
        where: {biodata_id: biodataUser.id},
        include: Games
      });

      res.status(201).json({
        data: scoreUser,
        message: "Success",
      });
    }
  } catch (error) {
    res.status(404).json({
      message: "FAILED",
    });
  }
};

exports.updateprofile = async (req, res) => {
  try {
    const profileUpdate = await Users.findOne({
      where: { id: req.user.id },
    });
    await profileUpdate.update(req.body);
    await profileUpdate.save();

    res.json({
      message: "SUKSES",
      data: profileUpdate,
    });
  } catch (error) {
    res.status(404).json({
      message: "FAILED",
    });
  }
};

exports.createbiodata = async (req, res) => {
  const payload = req.body;

  const profileBiodata = await Users.findOne({
    where: { id: req.user.id },
    include: Biodatas,
  });
  if (profileBiodata) {
    const biodata = await Biodatas.findOne({
      where: { user_id: req.user.id },
    });
    await biodata.update(payload);
    await biodata.save();
    res.json({
      message: "SUKSES",
      data: biodata,
    });
  } else {
    res.status(404).json({
      message: "FAILED",
    });
    return;
  }
};
